package com.cobblemon.mod.common.pokemon.effects

import com.cobblemon.mod.common.api.pokemon.effect.ShoulderEffect
import com.cobblemon.mod.common.pokemon.Pokemon
import com.cobblemon.mod.common.util.DataKeys.POKEMON_UUID
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.effect.StatusEffect
import net.minecraft.entity.effect.StatusEffectInstance
import net.minecraft.nbt.NbtCompound
import net.minecraft.server.network.ServerPlayerEntity
import java.util.*

 abstract class StatusShoulderEffect(private val effectType: StatusEffect) : ShoulderEffect {

    private class ShoulderEffectInstance(effectType: StatusEffect, pokemon: Pokemon) :
        StatusEffectInstance(effectType, 10, 0, true, false, false) {

        val pokemonIds: MutableList<UUID>

        init {
            pokemonIds = mutableListOf(pokemon.uuid)
        }

        private fun isShoulderedPokemon(shoulderEntity: NbtCompound): Boolean {
            val pkmNBT = shoulderEntity.getCompound("Pokemon")

            return pkmNBT.containsUuid(POKEMON_UUID) && pkmNBT.getUuid(POKEMON_UUID) in pokemonIds
        }

        fun isShoulderingPlayer(playerEntity: ServerPlayerEntity): Boolean {
            return arrayOf(playerEntity.shoulderEntityLeft, playerEntity.shoulderEntityRight).any {
                isShoulderedPokemon(it)
            }
        }

        override fun writeNbt(nbt: NbtCompound): NbtCompound {
            super.writeNbt(nbt)
            nbt.putInt("id", -999)
            return nbt
        }

        override fun update(entity: LivingEntity?, overwriteCallback: Runnable?): Boolean {
            entity as ServerPlayerEntity
            duration = if (isShoulderingPlayer(entity)) 10 else 0

            return super.update(entity, overwriteCallback)
        }
    }

    final override fun applyEffect(pokemon: Pokemon, player: ServerPlayerEntity, isLeft: Boolean) {

        val effect = player.statusEffects.filterIsInstance<ShoulderEffectInstance>().firstOrNull { e ->
            e.effectType == effectType
        }

        if (effect != null) {
            effect.pokemonIds.add(pokemon.uuid)
        } else {
            player.addStatusEffect(ShoulderEffectInstance(effectType, pokemon))
        }
    }

    final override fun removeEffect(pokemon: Pokemon, player: ServerPlayerEntity, isLeft: Boolean) {
        val effect = player.statusEffects.filterIsInstance<ShoulderEffectInstance>().firstOrNull { e ->
            e.effectType == effectType
        }
        effect?.pokemonIds?.remove(pokemon.uuid)

    }

}